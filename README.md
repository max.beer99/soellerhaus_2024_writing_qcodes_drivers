# Writing QCoDes drivers for physical instruments and for sample representation

This repo contains files relating to my presentation at the Soellerhaus institute conference 2024.

Note: The given jupyter notebooks create an sqlite db in the your `home/Downloads` folder.

## Requirements

Package requirements can be fulfilled on `python3.12` with the following `pip install` command:

~~~bash
pip install jupyterlab qcodes numpy xarray plotly pandas 
~~~

The Arduino R4 Wifi firmware required for `3_arduino_driver.ipynb` can be obtained from me on request. Additional requirements can be fulfilled by:

~~~bash
pip install pyvisa-py pyserial
~~~